package com.codeloaders.utility;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtility {

	private static XSSFSheet ExcelWSheet;

	private static XSSFWorkbook ExcelWBook;

	private static XSSFCell Cell;
	
	public static Object[][] getTestData(String FilePath, String SheetName,int startingRow,int lastRow,int column) throws Exception {
		
		String [][] table = null;
		
		FileInputStream excelFile = new FileInputStream(FilePath);
		
		ExcelWBook = new XSSFWorkbook(excelFile);

		ExcelWSheet = ExcelWBook.getSheet(SheetName);

		int i = startingRow;
		
		int k= lastRow;
		
		int j=column;
		
		int ci=0;int cj=0;
		
		int totalRows =k-i;
		
		int totalCols=1;
		
		table=new String[totalRows][totalCols];
		
		while(i<=k){
			
		table[ci][cj]=getCellData(i,j);			
		
		ci++ ;i++; 
		   
		}
		
		return (table);
	}

	 public static String getCellData(int RowNum, int ColNum) throws Exception {
		 
		 Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
		 
		 String CellData = Cell.getStringCellValue();
		 
		 return CellData;
}
	 
} 