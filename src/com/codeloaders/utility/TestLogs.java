package com.codeloaders.utility;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.BeforeMethod;

public class TestLogs {

	private static final Logger logger = Logger.getLogger(TestLogs.class.getName());
	
	@BeforeMethod

	public void loadTestLogs(){
	
	String log4jconfgPath = System.getProperty("/log4j.properties");
	
	PropertyConfigurator.configure(log4jconfgPath);
	
	logger.toString();
	
	}
	
}
