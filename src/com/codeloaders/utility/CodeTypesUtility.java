package com.codeloaders.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CodeTypesUtility {
	
	
	public static int getRowCount_CPT(String filePath){
		int rowCount =0;
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			String line;
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    }           
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		return rowCount-1;
	}
	
	public static List<String> getLine_CPT(String filePath, int rowNumber){
		String line = "";
		int rowCount = 0;
		List<String> data = null;
		
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			line = br.readLine();
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    	if(rowCount == rowNumber){
			    		break;
			    	}
			    }  
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		
		if(rowCount != 0){
			data = Arrays.asList(line.split("\\t"));
		}
		return data;
	}
	
	public static int getRowCount_HCPCS(String filePath){
		int rowCount =0;
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			String line;
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    }           
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		return rowCount-1;
	}
	
	public static List<String> getLine_HCPCS(String filePath, int rowNumber){
		String line = "";
		int rowCount = 0;
		List<String> data = null;
		
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			line = br.readLine();
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    	if(rowCount == rowNumber){
			    		break;
			    	}
			    }  
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		
		if(rowCount != 0){
			data = Arrays.asList(line.split("\\t"));
		}
		return data;
	}
	
	public static int getRowCount_ICD10DIAG(String filePath){
		int rowCount =0;
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			String line;
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    }           
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		return rowCount-1;
	}
	
	public static List<String> getLine_ICD10DIAG(String filePath, int rowNumber){
		String line = "";
		int rowCount = 0;
		List<String> data = null;
		
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			line = br.readLine();
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    	if(rowCount == rowNumber){
			    		break;
			    	}
			    }  
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		
		if(rowCount != 0){
			data = Arrays.asList(line.split("\\t"));
		}
		return data;
	}
	
	public static int getRowCount_ICD10PROC(String filePath){
		int rowCount =0;
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			String line;
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    }           
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		return rowCount-1;
	}
	
	public static List<String> getLine_ICD10PROC(String filePath, int rowNumber){
		String line = "";
		int rowCount = 0;
		List<String> data = null;
		
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			line = br.readLine();
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    	if(rowCount == rowNumber){
			    		break;
			    	}
			    }  
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		
		if(rowCount != 0){
			data = Arrays.asList(line.split("\\t"));
		}
		return data;
	}
	
	public static int getRowCount_ZIPTOCARRIERLOCALITY(String filePath){
		int rowCount =0;
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			String line;
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    }           
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		return rowCount-1;
	}
	
	public static List<String> getLine_ZIPTOCARRIERLOCALITY(String filePath, int rowNumber){
		String line = "";
		int rowCount = 0;
		List<String> data = null;
		
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			line = br.readLine();
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    	if(rowCount == rowNumber){
			    		break;
			    	}
			    }  
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		
		if(rowCount != 0){
			data = Arrays.asList(line.split("\\t"));
		}
		return data;
	}
	
	
	public static int getRowCount_ANESTHESIA(String filePath){
		int rowCount =0;
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			String line;
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    }           
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		return rowCount-1;
	}
	
	public static List<String> getLine_ANESTHESIA(String filePath, int rowNumber){
		String line = "";
		int rowCount = 0;
		List<String> data = null;
		
		try(BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))){
			line = br.readLine();
			while ((line = br.readLine()) != null) {
			    if(line.trim().length() > 0) {
			    	rowCount++;
			    	if(rowCount == rowNumber){
			    		break;
			    	}
			    }  
			}
		}catch(IOException ie){
			System.err.println("Unable to read given file : "+ie);
		}
		
		if(rowCount != 0){
			data = Arrays.asList(line.split("\\t"));
		}
		return data;
	}
	
	
	
	
}
	
	

