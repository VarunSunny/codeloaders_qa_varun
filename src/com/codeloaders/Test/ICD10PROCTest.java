package com.codeloaders.Test;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.codeloaders.utility.CodeTypesUtility;
import com.codeloaders.utility.ExcelUtility;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class ICD10PROCTest {
	
	ExtentReports logger;

    InputStream propertyFile = ICD10PROCTest.class.getResourceAsStream("/CodeLoaders_Configuration.properties");

	Properties property= new Properties();
	
	Connection codeLoaderDB;
	
	Connection payorDB;
	
	Statement sm1,sm2;
	
	ResultSet rs;
	
	long inputFileCount_ICD10PROC =0;
	
	int rowCountRawDataFile =0;
	
	List<String> sampleRawData_ICD10PROC=null; 
	
	List<String> updatedSampleRawData_ICD10PROC=null; 
	
	List<String> updatedSampleRawData_ICD10PROC_1=null; 
	
	List<String> updatedSampleRawData_ICD10PROC_2=null; 
	
	List<String> createDeleteSampleRawData_ICD10PROC_1=null; 
	
	List<String> createDeleteSampleRawData_ICD10PROC_2=null; 
	
	int rowCountCodeLoaderDB_ICD10PROC=0;
	
	int rowCountCodeLoaderDB_ICD10PROC_updated=0;
	
	int rowCountCodeLoaderDB_ICD10PROC_updated_2=0;
	
	int rowCountCodeLoader_ICD10PROC_updated=0;
	
	String sampleData_CodeLoaderDB_ICD10PROC =null;
	
	String updatedDataCodeloader_ICD10PROC =null;
	
	String updatedCodeLoaderData_1_ICD10PROC =null;
	
	String updatedCodeLoaderData_2_ICD10PROC =null;
	
	String  updatedCodeLoaderSampleData_1_ICD10PROC =null;
	
	String  updatedCodeLoaderSampleData_2_ICD10PROC =null;
	
	String createDeleteValue_CodeLoader=null;
	
	String createDeleteValue_CodeLoader_2=null;
	
	@BeforeClass
	
	public void inputDependencies_ICD10PROC() throws Exception{
		
		property.load(propertyFile);
		
		File inputFile_ICD10PROC = new File("inputFileICD10PROC");
		
		inputFileCount_ICD10PROC =inputFile_ICD10PROC.length();
		
		rowCountRawDataFile = CodeTypesUtility.getRowCount_ICD10PROC(property.getProperty("inputFile_ICD10PROC"));
		
		sampleRawData_ICD10PROC = CodeTypesUtility.getLine_ICD10PROC(property.getProperty("inputFile_ICD10PROC"), 1);
		
		 updatedSampleRawData_ICD10PROC=CodeTypesUtility.getLine_ICD10PROC(property.getProperty("inputFile_2_ICD10PROC"), 1);
		
		 updatedSampleRawData_ICD10PROC_1=CodeTypesUtility.getLine_ICD10PROC(property.getProperty("inputFile_2_ICD10PROC"), 2); 
		
		 updatedSampleRawData_ICD10PROC_2=CodeTypesUtility.getLine_ICD10PROC(property.getProperty("inputFile_2_ICD10PROC"), 2); 
		 
		 createDeleteSampleRawData_ICD10PROC_1 =CodeTypesUtility.getLine_ICD10PROC(property.getProperty("inputFile_3_ICD10PROC"), 1); 
		 
		 createDeleteSampleRawData_ICD10PROC_2=CodeTypesUtility.getLine_ICD10PROC(property.getProperty("inputFile_3_ICD10PROC"), 2); 
		
		codeLoaderDB =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","codeloaderdb","vega");  
		
		payorDB = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","payordb","vega");
		
		 sm1 = codeLoaderDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
		
		 sm2 = payorDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
	}
	
		
	@Test(priority=001,description="CodeLoader_2_verifyCreate_01",dataProvider="ICD10PROC_TestCase_01")
	
	public void CodeLoader_2_verifyCreate_01(String query1,String query2,String query3) throws Exception{
	
		logger = ExtentReports.get(ICD10PROCTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_2_verifyCreate_01");
		
		if (inputFileCount_ICD10PROC == 1){
		
		rs= sm1.executeQuery(query1); 
		
		rs.next();
		
		rowCountCodeLoaderDB_ICD10PROC = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_ICD10PROC, rowCountRawDataFile);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_ICD10PROC+"]"+"["
		+rowCountRawDataFile+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action, "CREATE");
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action+"]"+"["
				+"CREATE"+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
	   sampleData_CodeLoaderDB_ICD10PROC = rs.getString(1);
		
		Assert.assertEquals(sampleData_CodeLoaderDB_ICD10PROC,sampleRawData_ICD10PROC );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+sampleData_CodeLoaderDB_ICD10PROC+"]"+"["
				+sampleRawData_ICD10PROC+"]");
		
		}
		logger.endTest();
	
	}
	
	@DataProvider(name="ICD10PROC_TestCase_01")
	
	public Object[][] testData1() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "ICD10PROC", 3,5,3);
		
		return data;
		
		}
	
	@Test(priority=002,description="CodeLoader_2_verifyCreate_02",dataProvider="ICD10PROC_TestCase_02")
	
	public void CodeLoader_2_verifyCreate_02(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(ICD10PROCTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_2_verifyCreate_02");
		
		if (inputFileCount_ICD10PROC == 1){
		
		rs= sm2.executeQuery(query1); 
		
		rs.next();
		
		int rowCountPayorDB_ICD10PROC = rs.getInt(1);
		
		Assert.assertEquals(rowCountPayorDB_ICD10PROC , rowCountCodeLoaderDB_ICD10PROC);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_ICD10PROC+"]"+"["
		+rowCountCodeLoaderDB_ICD10PROC+"]");
	
		rs = sm2.executeQuery(query2);
		
		rs.next();
		
	   String sampleData_PayorDB_ICD10PROC = rs.getString(1);
		
		Assert.assertEquals(sampleData_PayorDB_ICD10PROC,sampleData_CodeLoaderDB_ICD10PROC );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+sampleData_CodeLoaderDB_ICD10PROC+"]"+"["
				+sampleData_CodeLoaderDB_ICD10PROC+"]");
		
		}
		logger.endTest();
	
	}
	
	@DataProvider(name="ICD10PROC_TestCase_02")
	
	public Object[][] testData2() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "ICD10PROC",6,7,3);
		
		return data;
		
		}
	
	@Test(priority=003,description="CodeLoader_12_verifyUpdate_01",dataProvider="ICD10PROC_TestCase_03")
	
	public void CodeLoader_12_verifyUpdate_01(String query1,String query2,String query3,String query4) throws Exception{
	
		logger = ExtentReports.get(ICD10PROCTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_01(");
		
		if (inputFileCount_ICD10PROC == 2){
		
		rs= sm1.executeQuery(query1); 
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action , "APPEND");
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action +"]"+"["
		+"APPEND"+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
		updatedDataCodeloader_ICD10PROC = rs.getString(1);
		
		Assert.assertEquals(updatedDataCodeloader_ICD10PROC, updatedSampleRawData_ICD10PROC);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataCodeloader_ICD10PROC+"]"+"["
				+updatedSampleRawData_ICD10PROC+"]");
	
		rs = sm1.executeQuery(query3);
		
		rs.next();
		
	     rowCountCodeLoaderDB_ICD10PROC_updated = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_ICD10PROC_updated,2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_ICD10PROC_updated+"]"+"["
				+2+"]");
		
		rs = sm1.executeQuery(query4);
		
		rs.next();
		
	    rowCountCodeLoaderDB_ICD10PROC_updated_2 = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_ICD10PROC_updated_2,2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_ICD10PROC_updated_2+"]"+"["
				+2+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="ICD10PROC_TestCase_03")
	
	public Object[][] testData3() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "ICD10PROC", 11, 14, 3);
		
		return data;
		
		}
	
	
	@Test(priority=004,description="CodeLoader_12_verifyUpdate_02",dataProvider="ICD10PROC_TestCase_04")
	
	public void CodeLoader_12_verifyUpdate_02(String query1,String query2,String query3,String query4) throws Exception{
	
		logger = ExtentReports.get(ICD10PROCTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_02");
		
		if (inputFileCount_ICD10PROC == 2){
		
			rs = sm2.executeQuery(query1);
			
			rs.next();
			
		    int rowCountPayorDB_ICD10PROC_updated = rs.getInt(1);
			
			Assert.assertEquals(rowCountPayorDB_ICD10PROC_updated,rowCountCodeLoaderDB_ICD10PROC_updated);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountPayorDB_ICD10PROC_updated+"]"+"["
					+rowCountCodeLoaderDB_ICD10PROC_updated+"]");
			
		rs= sm2.executeQuery(query2); 
		
		rs.next();
		
		String updatedDataPayor_ICD10PROC = rs.getString(1);
		
		Assert.assertEquals(updatedDataPayor_ICD10PROC ,updatedDataCodeloader_ICD10PROC );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataPayor_ICD10PROC  +"]"+"["
		+updatedDataCodeloader_ICD10PROC+"]");
	
		rs = sm2.executeQuery(query3);
		
		rs.next();
		
		int countupdatedDataPayor_ICD10PROC = rs.getInt(1);
		
		Assert.assertEquals(countupdatedDataPayor_ICD10PROC, rowCountCodeLoaderDB_ICD10PROC_updated);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+countupdatedDataPayor_ICD10PROC+"]"+"["
				+rowCountCodeLoaderDB_ICD10PROC_updated+"]");
	
		rs= sm2.executeQuery(query4); 
		
		rs.next();
		
		String updatedDataPayor_2_ICD10PROC = rs.getString(1);
		
		Assert.assertEquals(updatedDataPayor_2_ICD10PROC ,updatedDataPayor_2_ICD10PROC);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataPayor_2_ICD10PROC+"]"+"["
		+updatedDataPayor_2_ICD10PROC+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="ICD10PROC_TestCase_04")
	
	public Object[][] testData4() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "ICD10PROC", 16, 19, 3);
		
		return data;
		
		}
	
	@Test(priority=005,description="CodeLoader_12_verifyUpdate_03",dataProvider="ICD10PROC_TestCase_05")
	
	public void CodeLoader_12_verifyUpdate_03(String query1,String query2,String query3,String query4,String query5) throws Exception{
	
		logger = ExtentReports.get(ICD10PROCTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_02");
		
		if (inputFileCount_ICD10PROC == 2){
		
			rs = sm1.executeQuery(query1);
			
			rs.next();
			
		     rowCountCodeLoader_ICD10PROC_updated = rs.getInt(1);
			
			Assert.assertEquals(rowCountCodeLoader_ICD10PROC_updated,5);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoader_ICD10PROC_updated+"]"+"["
					+5+"]");
			
		rs= sm1.executeQuery(query2); 
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action ,"CREATE" );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action +"]"+"["
		+"CREATE"+"]");
	
		rs= sm1.executeQuery(query3); 
		
		rs.next();
		
		String action2 = rs.getString(1);
		
		Assert.assertEquals(action2 ,"CREATE" );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action2 +"]"+"["
		+"CREATE"+"]");
		
		
		rs= sm1.executeQuery(query4); 
		
		rs.next();
		
		updatedCodeLoaderSampleData_1_ICD10PROC = rs.getString(1);
		
		Assert.assertEquals(updatedCodeLoaderData_1_ICD10PROC ,updatedSampleRawData_ICD10PROC_1);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_1_ICD10PROC+"]"+"["
		+updatedSampleRawData_ICD10PROC_1+"]");
		
		rs= sm1.executeQuery(query5); 
		
		rs.next();
		
		updatedCodeLoaderSampleData_2_ICD10PROC = rs.getString(1);
		
		Assert.assertEquals(updatedCodeLoaderData_2_ICD10PROC ,updatedSampleRawData_ICD10PROC_2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_2_ICD10PROC+"]"+"["
		+updatedSampleRawData_ICD10PROC_2+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="ICD10PROC_TestCase_05")
	
	public Object[][] testData5() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "ICD10PROC",20,24,3);
		
		return data;
		
		}
	
	
	
	@Test(priority=006,description="CodeLoader_12_verifyUpdate_04",dataProvider="ICD10PROC_TestCase_06")
	
	public void CodeLoader_12_verifyUpdate_04(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(ICD10PROCTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_04");
		
		if (inputFileCount_ICD10PROC == 2){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String updatedPayorSampleData_1_ICD10PROC = rs.getString(1);
			
			Assert.assertEquals(updatedPayorSampleData_1_ICD10PROC ,updatedCodeLoaderData_1_ICD10PROC);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedPayorSampleData_1_ICD10PROC+"]"+"["
			+updatedCodeLoaderData_1_ICD10PROC+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			String updatedPayorSampleData_2_ICD10PROC = rs.getString(1);
			
			Assert.assertEquals(updatedPayorSampleData_2_ICD10PROC,updatedCodeLoaderData_2_ICD10PROC);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_2_ICD10PROC+"]"+"["
			+updatedCodeLoaderData_2_ICD10PROC+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="ICD10PROC_TestCase_06")
	
	public Object[][] testData6() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "ICD10PROC", 25, 26, 3);
		
		return data;
		
		}	
	
}
