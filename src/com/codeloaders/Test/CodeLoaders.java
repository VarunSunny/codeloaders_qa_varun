package com.codeloaders.Test;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class CodeLoaders  {
	
	ExtentReports logger;
		
	Properties property;

	Process p;
	
	Connection codeLoaderDB;
	
	Connection payorDB;
	
	ResultSet rs;
	
	@BeforeSuite(description="CodeLoader_1_PreTestSetUp")
	
	public void PreTestSetUp() throws Exception{
		
	// Loading the values/ data from config.properties file
		
	InputStream propertyFile = CodeLoaders.class.getResourceAsStream("/CodeLoaders_Configuration.properties");

	Properties property;
		
	property= new Properties();
		
	property.load(propertyFile);	
		
	// Delete the Application.properties file in CodeLoaders Application
		
	File applicationProperties = new File(property.getProperty("applicationPropertiesPath"));
		
	applicationProperties.delete();
		
	// Create the Application.properties file in CodeLoaders Application with details of the codeloaderdb and other details required
		
	Properties property2 = new Properties();
		
	InputStream applicationPropertyFile = CodeLoaders.class.getResourceAsStream("/CodeLoaders_application.properties");

	property2.load(applicationPropertyFile);
		
	OutputStream sample1 = null;
		
	sample1 = new FileOutputStream(property.getProperty("applicationPropertiesPath"));
		
	property2.store(sample1, null);
	
	Properties property3 = new Properties();
	
	InputStream environment_specifi_PropertyFile = CodeLoaders.class.getResourceAsStream("/CodeLoaders_application.properties");

	property3.load(environment_specifi_PropertyFile);
		
	OutputStream sample2 = null;
		
	sample2 = new FileOutputStream(property.getProperty("environment_specificPropertiesPath"));
		
	property3.store(sample2, null);
	
	logger = ExtentReports.get(CodeLoaders.class);
		
	logger.init(property.getProperty("ExtentReportsPath"), false);
		
	logger.startTest("PreTestSetup");
		
	// Find the initial count in the codeloaderdb and payordb.
		
	Class.forName("oracle.jdbc.driver.OracleDriver");  
		
	Connection codeloaderDB =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","codeloaderdb","vega");  
		
	Connection payorDB = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","payordb","vega");
		
	Statement sm1 = codeloaderDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
		
	Statement sm2 = payorDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
		
	ResultSet rs1= sm1.executeQuery("select count(*) from t_code_file_status");
		
	rs1.next();
		
	int countCodeTypeCDB_Initial = rs1.getInt(1);
		
	Assert.assertEquals(countCodeTypeCDB_Initial, 0);
	
	System.out.println(countCodeTypeCDB_Initial);
		
	logger.log(LogStatus.INFO, "The initial count T_CODE_FILE_STATUS in of CodeloaderDB befor launching the Application is : "+countCodeTypeCDB_Initial);
		
	rs1= sm1.executeQuery("select count(*) from t_service");
		
	rs1.next();
		
	int countCDB_Initial = rs1.getInt(1);
	
	System.out.println(countCDB_Initial);
		
	Assert.assertEquals(countCDB_Initial, 0);
		
	logger.log(LogStatus.INFO, "The initial count of T_SERVICE in CodeloaderDB befor launching the Application is : "+countCodeTypeCDB_Initial);
		
		rs1= sm2.executeQuery("select count(*) from service");
		
		rs1.next();
		
		int payorCDB_Initial = rs1.getInt(1);
		
		System.out.println(payorCDB_Initial);
		
		Assert.assertEquals(payorCDB_Initial, 0);
		
		logger.log(LogStatus.INFO, "The initial count of SERVICE in PayorDB befor launching the Application is : "+countCodeTypeCDB_Initial);
		
		sm1.close();
		
		sm2.close();
		
		codeloaderDB.close();
		
		payorDB.close();

		String[] startCommand = {"cmd.exe", "/C", "Start",property.getProperty("codeLoaders_Path")};
		
		p = Runtime.getRuntime().exec(startCommand);
			
		logger.log(LogStatus.INFO, "CodeLoaders Application has been Launched Successfully");
		
		logger.endTest();
		
		}
	
	@AfterMethod
	public void getResult(ITestResult result){
		
	    if(result.getStatus()==ITestResult.FAILURE){
	   
	    logger.log(LogStatus.FAIL," Verification of" + result.getName()+ "is Failed");
	    	
	    }else{
	    	
	    logger.log(LogStatus.PASS," Verification of" + result.getName()+ "is SuccessFul");
	    
	    }
	}
	
	    
	    @AfterSuite

		public void closeCodeLoaders() throws Exception{
		
		InputStream propertyFile = CodeLoaders.class.getResourceAsStream("/CodeLoaders_Configuration.properties");

		Properties property;
		
		property= new Properties();
		
		property.load(propertyFile);
		
		logger = ExtentReports.get(CodeLoaders.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("closeCodeLoaders");
		
		p.waitFor(60, TimeUnit.SECONDS);
		
		p.destroyForcibly();
		
		logger.log(LogStatus.INFO, "Codeloaders Application has been closed successfully");
		
		Thread.sleep(5000);
		
		Runtime.getRuntime().exec("taskkill /IM cmd.exe");
	
		Class.forName("oracle.jdbc.driver.OracleDriver");  
		
		 codeLoaderDB =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","codeloaderdb","vega");  
		
		 payorDB = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","payordb","vega");
		
		Statement sm1 = codeLoaderDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
		
		Statement sm2 = payorDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
		
		rs= sm1.executeQuery("delete from t_code_file_status");
		
		rs=sm1.executeQuery("delete from t_service");
		
		rs=sm1.executeQuery("delete from t_diagnosis");
		
		rs=sm1.executeQuery("delete from t_procedure");
		
		rs=sm1.executeQuery("delete from t_zip_to_carrier_locality");
		
		rs=sm1.executeQuery("delete from t_anesthesia_conversion_factor");
		
		logger.log(LogStatus.INFO, "Tables from Codeloader DB has been dropped successfully");
		
		rs=sm2.executeQuery("delete from service");
		
		rs=sm2.executeQuery("delete from diagnosis");
		
		rs=sm2.executeQuery("delete from procedure");
		
		rs=sm2.executeQuery("delete from zip_to_carrier_locality");
		
		rs=sm2.executeQuery("delete from anesthesia_conversion_factor");
		
		logger.log(LogStatus.INFO, "Tables from payor DB has been dropped successfully");
		
		logger.log(LogStatus.INFO, "Exited from the Run time Window Successfully");
		
		logger.endTest();
		
		}
	
}	
