package com.codeloaders.Test;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.codeloaders.utility.CodeTypesUtility;
import com.codeloaders.utility.ExcelUtility;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class CPTTest  {
	
	ExtentReports logger;

    InputStream propertyFile = CPTTest.class.getResourceAsStream("/CodeLoaders_Configuration.properties");

	Properties property= new Properties();
	
	Connection codeLoaderDB;
	
	Connection payorDB;
	
	Statement sm1,sm2;
	
	ResultSet rs;
	
	long inputFileCount_CPT =0;
	
	int rowCount_CPT_01 =0;
	
	String sampleRawData_file_01_CPT=null; 
	
	String updatedSampleRawData_file_02_CPT=null; 
	
	
	
	
	
	
	
	
	
	
	
	List<String> updatedSampleRawData_file_02_CPT_1=null; 
	
	List<String> updatedSampleRawData_file_02_CPT_2=null; 
	
	List<String> createDeletesampleRawData_file_01_CPT_1=null; 
	
	List<String> createDeletesampleRawData_file_01_CPT_2=null; 
	
	int rowCountCodeLoaderDB_CPT=0;
	
	int rowCountCodeLoaderDB_CPT_updated=0;
	
	int rowCountCodeLoaderDB_CPT_updated_2=0;
	
	int rowCountCodeLoader_CPT_updated=0;
	
	String sampleData_CodeLoaderDB_CPT =null;
	
	String updatedDataCodeloader_CPT =null;
	
	String updatedCodeLoaderData_1_CPT =null;
	
	String updatedCodeLoaderData_2_CPT =null;
	
	String  updatedCodeLoaderSampleData_1_CPT =null;
	
	String  updatedCodeLoaderSampleData_2_CPT =null;
	
	String createDeleteValue_CodeLoader=null;
	
	String createDeleteValue_CodeLoader_2=null;
			
	@BeforeClass
	
	public void inputDependencies_CPT() throws Exception{
		
	property.load(propertyFile);
	
	Map<String,Integer> rowCountMap_File1_CPT = new HashMap<>();
		
	File inputFolder_CPT = new File(property.getProperty("CPT_baseData_path"));
	
	File [] CPT_files = inputFolder_CPT.listFiles();
	
	inputFileCount_CPT = inputFolder_CPT.listFiles().length;
	
	for(File file1 : CPT_files){
		
	rowCountMap_File1_CPT.put(file1.getName(), CodeTypesUtility.getRowCount_CPT(file1.getAbsolutePath()));
	
	}
	
	rowCount_CPT_01=rowCountMap_File1_CPT.get(property.getProperty("file_01_CPT_Path"));
		
	sampleRawData_file_01_CPT = CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_01_CPT"), 1).get(2);
	
	updatedSampleRawData_file_02_CPT = CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_02_CPT"), 2).get(2);
	
	
	
	
		
	
	
	
	
	
	
	
 
		
	updatedSampleRawData_file_02_CPT_1=CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_2_CPT"), 2); 
		
	updatedSampleRawData_file_02_CPT_2=CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_2_CPT"), 2); 
		 
	createDeletesampleRawData_file_01_CPT_1 =CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_3_CPT"), 1); 
		 
	createDeletesampleRawData_file_01_CPT_2=CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_3_CPT"), 2); 
		
		codeLoaderDB =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","codeloaderdb","vega");  
		
		payorDB = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","payordb","vega");
		
		 sm1 = codeLoaderDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
		
		 sm2 = payorDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
	}
	
		
	@Test(priority=001,description="CodeLoader_2_verifyCreate_01",dataProvider="CPT_TestCase_01")
	
	public void CodeLoader_2_verifyCreate_01(String query1,String query2,String query3) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_2_verifyCreate_01");
		
		if (inputFileCount_CPT == 1){
		
		rs= sm1.executeQuery(query1); 
		
		rs.next();
		
		rowCountCodeLoaderDB_CPT = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_CPT, rowCount_CPT_01);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_CPT+"]"+"["
		+rowCount_CPT_01+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action, "CREATE");
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action+"]"+"["
				+"CREATE"+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
	   sampleData_CodeLoaderDB_CPT = rs.getString(1);
		
		Assert.assertEquals(sampleData_CodeLoaderDB_CPT,sampleRawData_file_01_CPT );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+sampleData_CodeLoaderDB_CPT+"]"+"["
				+sampleRawData_file_01_CPT+"]");
		
		}
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_01")
	
	public Object[][] testData1() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 3,5,3);
		
		return data;
		
		}
	
	@Test(priority=002,description="CodeLoader_2_verifyCreate_02",dataProvider="CPT_TestCase_02")
	
	public void CodeLoader_2_verifyCreate_02(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_2_verifyCreate_02");
		
		if (inputFileCount_CPT == 1){
		
		rs= sm2.executeQuery(query1); 
		
		rs.next();
		
		int rowCountPayorDB_CPT = rs.getInt(1);
		
		Assert.assertEquals(rowCountPayorDB_CPT , rowCountCodeLoaderDB_CPT);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_CPT+"]"+"["
		+rowCountCodeLoaderDB_CPT+"]");
	
		rs = sm2.executeQuery(query2);
		
		rs.next();
		
	   String sampleData_PayorDB_CPT = rs.getString(1);
		
		Assert.assertEquals(sampleData_PayorDB_CPT,sampleData_CodeLoaderDB_CPT );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+sampleData_CodeLoaderDB_CPT+"]"+"["
				+sampleData_CodeLoaderDB_CPT+"]");
		
		}
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_02")
	
	public Object[][] testData2() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT",6,7,3);
		
		return data;
		
		}
	
	@Test(priority=003,description="CodeLoader_12_verifyUpdate_01",dataProvider="CPT_TestCase_03")
	
	public void CodeLoader_12_verifyUpdate_01(String query1,String query2,String query3,String query4) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_01(");
		
		if (inputFileCount_CPT == 2){
		
		rs= sm1.executeQuery(query1); 
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action , "APPEND");
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action +"]"+"["
		+"APPEND"+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
		updatedDataCodeloader_CPT = rs.getString(1);
		
		Assert.assertEquals(updatedDataCodeloader_CPT, updatedSampleRawData_file_02_CPT);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataCodeloader_CPT+"]"+"["
				+updatedSampleRawData_file_02_CPT+"]");
	
		rs = sm1.executeQuery(query3);
		
		rs.next();
		
	     rowCountCodeLoaderDB_CPT_updated = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_CPT_updated,2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_CPT_updated+"]"+"["
				+2+"]");
		
		rs = sm1.executeQuery(query4);
		
		rs.next();
		
	    rowCountCodeLoaderDB_CPT_updated_2 = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_CPT_updated_2,2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_CPT_updated_2+"]"+"["
				+2+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_03")
	
	public Object[][] testData3() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 11, 14, 3);
		
		return data;
		
		}
	
	
	@Test(priority=004,description="CodeLoader_12_verifyUpdate_02",dataProvider="CPT_TestCase_04")
	
	public void CodeLoader_12_verifyUpdate_02(String query1,String query2,String query3,String query4) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_02");
		
		if (inputFileCount_CPT == 2){
		
			rs = sm2.executeQuery(query1);
			
			rs.next();
			
		    int rowCountPayorDB_CPT_updated = rs.getInt(1);
			
			Assert.assertEquals(rowCountPayorDB_CPT_updated,rowCountCodeLoaderDB_CPT_updated);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountPayorDB_CPT_updated+"]"+"["
					+rowCountCodeLoaderDB_CPT_updated+"]");
			
		rs= sm2.executeQuery(query2); 
		
		rs.next();
		
		String updatedDataPayor_CPT = rs.getString(1);
		
		Assert.assertEquals(updatedDataPayor_CPT ,updatedDataCodeloader_CPT );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataPayor_CPT  +"]"+"["
		+updatedDataCodeloader_CPT+"]");
	
		rs = sm2.executeQuery(query3);
		
		rs.next();
		
		int countupdatedDataPayor_CPT = rs.getInt(1);
		
		Assert.assertEquals(countupdatedDataPayor_CPT, rowCountCodeLoaderDB_CPT_updated);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+countupdatedDataPayor_CPT+"]"+"["
				+rowCountCodeLoaderDB_CPT_updated+"]");
	
		rs= sm2.executeQuery(query4); 
		
		rs.next();
		
		String updatedDataPayor_2_CPT = rs.getString(1);
		
		Assert.assertEquals(updatedDataPayor_2_CPT ,updatedDataPayor_2_CPT);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataPayor_2_CPT+"]"+"["
		+updatedDataPayor_2_CPT+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_04")
	
	public Object[][] testData4() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 16, 19, 3);
		
		return data;
		
		}
	
	@Test(priority=005,description="CodeLoader_12_verifyUpdate_03",dataProvider="CPT_TestCase_05")
	
	public void CodeLoader_12_verifyUpdate_03(String query1,String query2,String query3,String query4,String query5) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_02");
		
		if (inputFileCount_CPT == 2){
		
			rs = sm1.executeQuery(query1);
			
			rs.next();
			
		     rowCountCodeLoader_CPT_updated = rs.getInt(1);
			
			Assert.assertEquals(rowCountCodeLoader_CPT_updated,5);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoader_CPT_updated+"]"+"["
					+5+"]");
			
		rs= sm1.executeQuery(query2); 
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action ,"CREATE" );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action +"]"+"["
		+"CREATE"+"]");
	
		rs= sm1.executeQuery(query3); 
		
		rs.next();
		
		String action2 = rs.getString(1);
		
		Assert.assertEquals(action2 ,"CREATE" );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action2 +"]"+"["
		+"CREATE"+"]");
		
		
		rs= sm1.executeQuery(query4); 
		
		rs.next();
		
		updatedCodeLoaderSampleData_1_CPT = rs.getString(1);
		
		Assert.assertEquals(updatedCodeLoaderData_1_CPT ,updatedSampleRawData_file_02_CPT_1);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_1_CPT+"]"+"["
		+updatedSampleRawData_file_02_CPT_1+"]");
		
		rs= sm1.executeQuery(query5); 
		
		rs.next();
		
		updatedCodeLoaderSampleData_2_CPT = rs.getString(1);
		
		Assert.assertEquals(updatedCodeLoaderData_2_CPT ,updatedSampleRawData_file_02_CPT_2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_2_CPT+"]"+"["
		+updatedSampleRawData_file_02_CPT_2+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_05")
	
	public Object[][] testData5() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT",20,24,3);
		
		return data;
		
		}
	
	
	
	@Test(priority=006,description="CodeLoader_12_verifyUpdate_04",dataProvider="CPT_TestCase_06")
	
	public void CodeLoader_12_verifyUpdate_04(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_04");
		
		if (inputFileCount_CPT == 2){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String updatedPayorSampleData_1_CPT = rs.getString(1);
			
			Assert.assertEquals(updatedPayorSampleData_1_CPT ,updatedCodeLoaderData_1_CPT);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedPayorSampleData_1_CPT+"]"+"["
			+updatedCodeLoaderData_1_CPT+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			String updatedPayorSampleData_2_CPT = rs.getString(1);
			
			Assert.assertEquals(updatedPayorSampleData_2_CPT,updatedCodeLoaderData_2_CPT);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_2_CPT+"]"+"["
			+updatedCodeLoaderData_2_CPT+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_06")
	
	public Object[][] testData6() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 25, 26, 3);
		
		return data;
		
		}	
	
	@Test(priority=007,description="CodeLoader_23_verifyDelete_01",dataProvider="CPT_TestCase_07")
	
	public void CodeLoader_23_verifyDelete_01(String query1,String query2,String query3) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_23_verifyDelete_01");
		
		if (inputFileCount_CPT == 3){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String action = rs.getString(1);
			
			Assert.assertEquals(action ,"TERMINATE");
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action+"]"+"["
			+"TERMINATE"+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			int deleteCount= rs.getInt(1);
			
			Assert.assertEquals(deleteCount,3);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+deleteCount+"]"+"["
			+3+"]");
			
			rs= sm1.executeQuery(query3); 
			
			rs.next();
			
			String effEndDate = rs.getString(1);
			
			Assert.assertNotEquals(effEndDate ,null);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+effEndDate+"]"+"["
			+null+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_07")
	
	public Object[][] testData7() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 32,34,3);
		
		return data;
		
		}	
	
	@Test(priority=8,description="CodeLoader_23_verifyDelete_02",dataProvider="CPT_TestCase_08")
	
	public void CodeLoader_23_verifyDelete_02(String query1) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_23_verifyDelete_02");
		
		if (inputFileCount_CPT == 3){
				
			rs= sm2.executeQuery(query1); 
			
			rs.next();
			
			String effEndDate = rs.getString(1);
			
			Assert.assertNotEquals(effEndDate ,null);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+effEndDate+"]"+"["
			+null+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_08")
	
	public Object[][] testData8() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 35,35,3);
		
		return data;
		
		}	
	
	@Test(priority=9,description="CodeLoader_23_verifyDelete_03",dataProvider="CPT_TestCase_09")
	
	public void CodeLoader_23_verifyDelete_03(String query1,String query2,String query3,String query4) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_23_verifyDelete_03");
		
		if (inputFileCount_CPT == 3){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String action = rs.getString(1);
			
			Assert.assertEquals(action ,"CREATE");
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action+"]"+"["
			+"CREATE"+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			createDeleteValue_CodeLoader= rs.getString(1);
			
			Assert.assertEquals(createDeleteValue_CodeLoader,createDeletesampleRawData_file_01_CPT_1);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+createDeleteValue_CodeLoader+"]"+"["
			+createDeletesampleRawData_file_01_CPT_1+"]");
			
			rs= sm1.executeQuery(query3); 
			
			rs.next();
			
			String action2 = rs.getString(1);
			
			Assert.assertEquals(action ,"CREATE");
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action2+"]"+"["
			+"CREATE"+"]");
			
			rs= sm1.executeQuery(query4); 
			
			rs.next();
			
			createDeleteValue_CodeLoader_2= rs.getString(1);
			
			Assert.assertEquals(createDeleteValue_CodeLoader,createDeletesampleRawData_file_01_CPT_2);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+createDeleteValue_CodeLoader_2+"]"+"["
			+createDeletesampleRawData_file_01_CPT_2+"]");
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_09")
	
	public Object[][] testData9() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 36,39,3);
		
		return data;
		
		}
	

	@Test(priority=10,description="CodeLoader_23_verifyDelete_04",dataProvider="CPT_TestCase_10")
	
	public void CodeLoader_23_verifyDelete_04(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_23_verifyDelete_04");
		
		if (inputFileCount_CPT == 3){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String createDeleteValue_Payor= rs.getString(1);
			
			Assert.assertEquals(createDeleteValue_Payor,createDeleteValue_CodeLoader);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+createDeleteValue_Payor+"]"+"["
			+createDeleteValue_CodeLoader+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			String createDeleteValue_2_Payor= rs.getString(1);
			
			Assert.assertEquals(createDeleteValue_2_Payor,createDeleteValue_CodeLoader_2);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+createDeleteValue_2_Payor+"]"+"["
			+createDeleteValue_CodeLoader_2+"]");
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_10")
	
	public Object[][] testData10() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 40,41,3);
		
		return data;
		
		}
	
	
}
	
	
	

	
	
	
	 