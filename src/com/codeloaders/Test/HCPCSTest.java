
package com.codeloaders.Test;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.codeloaders.utility.CodeTypesUtility;
import com.codeloaders.utility.ExcelUtility;
import com.codeloaders.utility.TestLogs;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class HCPCSTest extends TestLogs {
	 
	ExtentReports logger;

    InputStream propertyFile = HCPCSTest.class.getResourceAsStream("/CodeLoaders_Configuration.properties");

	Properties property= new Properties();
	
	Connection codeLoaderDB;
	
	Connection payorDB;
	
	Statement sm1,sm2;
	
	ResultSet rs;
	
	long inputFileCount_HCPCS =0;
	
	int rowCountRawDataFile =0;
	
	List<String> sampleRawData_HCPCS=null; 
	
	List<String> updatedSampleRawData_HCPCS=null; 
	
	List<String> updatedSampleRawData_HCPCS_1=null; 
	
	List<String> updatedSampleRawData_HCPCS_2=null; 
	
	List<String> createDeleteSampleRawData_HCPCS_1=null; 
	
	List<String> createDeleteSampleRawData_HCPCS_2=null; 
	
	int rowCountCodeLoaderDB_HCPCS=0;
	
	int rowCountCodeLoaderDB_HCPCS_updated=0;
	
	int rowCountCodeLoaderDB_HCPCS_updated_2=0;
	
	int rowCountCodeLoader_HCPCS_updated=0;
	
	String sampleData_CodeLoaderDB_HCPCS =null;
	
	String updatedDataCodeloader_HCPCS =null;
	
	String updatedCodeLoaderData_1_HCPCS =null;
	
	String updatedCodeLoaderData_2_HCPCS =null;
	
	String  updatedCodeLoaderSampleData_1_HCPCS =null;
	
	String  updatedCodeLoaderSampleData_2_HCPCS =null;
	
	String createDeleteValue_CodeLoader=null;
	
	String createDeleteValue_CodeLoader_2=null;
	
	@BeforeClass
	
	public void inputDependencies_HCPCS() throws Exception{
		
		property.load(propertyFile);
		
		File inputFile_HCPCS = new File("inputFileHCPCS");
		
		inputFileCount_HCPCS =inputFile_HCPCS.length();
		
		rowCountRawDataFile = CodeTypesUtility.getRowCount_HCPCS(property.getProperty("inputFile_HCPCS"));
		
		sampleRawData_HCPCS = CodeTypesUtility.getLine_HCPCS(property.getProperty("inputFile_HCPCS"), 1);
		
		 updatedSampleRawData_HCPCS=CodeTypesUtility.getLine_HCPCS(property.getProperty("inputFile_2_HCPCS"), 1);
		
		 updatedSampleRawData_HCPCS_1=CodeTypesUtility.getLine_HCPCS(property.getProperty("inputFile_2_HCPCS"), 2); 
		
		 updatedSampleRawData_HCPCS_2=CodeTypesUtility.getLine_HCPCS(property.getProperty("inputFile_2_HCPCS"), 2); 
		 
		 createDeleteSampleRawData_HCPCS_1 =CodeTypesUtility.getLine_HCPCS(property.getProperty("inputFile_3_HCPCS"), 1); 
		 
		 createDeleteSampleRawData_HCPCS_2=CodeTypesUtility.getLine_HCPCS(property.getProperty("inputFile_3_HCPCS"), 2); 
		
		codeLoaderDB =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","codeloaderdb","vega");  
		
		payorDB = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","payordb","vega");
		
		 sm1 = codeLoaderDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
		
		 sm2 = payorDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
	}
	
		
	@Test(priority=001,description="CodeLoader_2_verifyCreate_01",dataProvider="HCPCS_TestCase_01")
	
	public void CodeLoader_2_verifyCreate_01(String query1,String query2,String query3) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_2_verifyCreate_01");
		
		if (inputFileCount_HCPCS == 1){
		
		rs= sm1.executeQuery(query1); 
		
		rs.next();
		
		rowCountCodeLoaderDB_HCPCS = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_HCPCS, rowCountRawDataFile);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_HCPCS+"]"+"["
		+rowCountRawDataFile+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action, "CREATE");
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action+"]"+"["
				+"CREATE"+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
	   sampleData_CodeLoaderDB_HCPCS = rs.getString(1);
		
		Assert.assertEquals(sampleData_CodeLoaderDB_HCPCS,sampleRawData_HCPCS );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+sampleData_CodeLoaderDB_HCPCS+"]"+"["
				+sampleRawData_HCPCS+"]");
		
		}
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_01")
	
	public Object[][] testData1() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS", 3,5,3);
		
		return data;
		
		}
	
	@Test(priority=002,description="CodeLoader_2_verifyCreate_02",dataProvider="HCPCS_TestCase_02")
	
	public void CodeLoader_2_verifyCreate_02(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_2_verifyCreate_02");
		
		if (inputFileCount_HCPCS == 1){
		
		rs= sm2.executeQuery(query1); 
		
		rs.next();
		
		int rowCountPayorDB_HCPCS = rs.getInt(1);
		
		Assert.assertEquals(rowCountPayorDB_HCPCS , rowCountCodeLoaderDB_HCPCS);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_HCPCS+"]"+"["
		+rowCountCodeLoaderDB_HCPCS+"]");
	
		rs = sm2.executeQuery(query2);
		
		rs.next();
		
	   String sampleData_PayorDB_HCPCS = rs.getString(1);
		
		Assert.assertEquals(sampleData_PayorDB_HCPCS,sampleData_CodeLoaderDB_HCPCS );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+sampleData_CodeLoaderDB_HCPCS+"]"+"["
				+sampleData_CodeLoaderDB_HCPCS+"]");
		
		}
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_02")
	
	public Object[][] testData2() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS",6,7,3);
		
		return data;
		
		}
	
	@Test(priority=003,description="CodeLoader_12_verifyUpdate_01",dataProvider="HCPCS_TestCase_03")
	
	public void CodeLoader_12_verifyUpdate_01(String query1,String query2,String query3,String query4) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_01(");
		
		if (inputFileCount_HCPCS == 2){
		
		rs= sm1.executeQuery(query1); 
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action , "APPEND");
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action +"]"+"["
		+"APPEND"+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
		updatedDataCodeloader_HCPCS = rs.getString(1);
		
		Assert.assertEquals(updatedDataCodeloader_HCPCS, updatedSampleRawData_HCPCS);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataCodeloader_HCPCS+"]"+"["
				+updatedSampleRawData_HCPCS+"]");
	
		rs = sm1.executeQuery(query3);
		
		rs.next();
		
	     rowCountCodeLoaderDB_HCPCS_updated = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_HCPCS_updated,2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_HCPCS_updated+"]"+"["
				+2+"]");
		
		rs = sm1.executeQuery(query4);
		
		rs.next();
		
	    rowCountCodeLoaderDB_HCPCS_updated_2 = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_HCPCS_updated_2,2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_HCPCS_updated_2+"]"+"["
				+2+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_03")
	
	public Object[][] testData3() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS", 11, 14, 3);
		
		return data;
		
		}
	
	
	@Test(priority=004,description="CodeLoader_12_verifyUpdate_02",dataProvider="HCPCS_TestCase_04")
	
	public void CodeLoader_12_verifyUpdate_02(String query1,String query2,String query3,String query4) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_02");
		
		if (inputFileCount_HCPCS == 2){
		
			rs = sm2.executeQuery(query1);
			
			rs.next();
			
		    int rowCountPayorDB_HCPCS_updated = rs.getInt(1);
			
			Assert.assertEquals(rowCountPayorDB_HCPCS_updated,rowCountCodeLoaderDB_HCPCS_updated);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountPayorDB_HCPCS_updated+"]"+"["
					+rowCountCodeLoaderDB_HCPCS_updated+"]");
			
		rs= sm2.executeQuery(query2); 
		
		rs.next();
		
		String updatedDataPayor_HCPCS = rs.getString(1);
		
		Assert.assertEquals(updatedDataPayor_HCPCS ,updatedDataCodeloader_HCPCS );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataPayor_HCPCS  +"]"+"["
		+updatedDataCodeloader_HCPCS+"]");
	
		rs = sm2.executeQuery(query3);
		
		rs.next();
		
		int countupdatedDataPayor_HCPCS = rs.getInt(1);
		
		Assert.assertEquals(countupdatedDataPayor_HCPCS, rowCountCodeLoaderDB_HCPCS_updated);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+countupdatedDataPayor_HCPCS+"]"+"["
				+rowCountCodeLoaderDB_HCPCS_updated+"]");
	
		rs= sm2.executeQuery(query4); 
		
		rs.next();
		
		String updatedDataPayor_2_HCPCS = rs.getString(1);
		
		Assert.assertEquals(updatedDataPayor_2_HCPCS ,updatedDataPayor_2_HCPCS);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedDataPayor_2_HCPCS+"]"+"["
		+updatedDataPayor_2_HCPCS+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_04")
	
	public Object[][] testData4() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS", 16, 19, 3);
		
		return data;
		
		}
	
	@Test(priority=005,description="CodeLoader_12_verifyUpdate_03",dataProvider="HCPCS_TestCase_05")
	
	public void CodeLoader_12_verifyUpdate_03(String query1,String query2,String query3,String query4,String query5) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_02");
		
		if (inputFileCount_HCPCS == 2){
		
			rs = sm1.executeQuery(query1);
			
			rs.next();
			
		     rowCountCodeLoader_HCPCS_updated = rs.getInt(1);
			
			Assert.assertEquals(rowCountCodeLoader_HCPCS_updated,5);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoader_HCPCS_updated+"]"+"["
					+5+"]");
			
		rs= sm1.executeQuery(query2); 
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action ,"CREATE" );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action +"]"+"["
		+"CREATE"+"]");
	
		rs= sm1.executeQuery(query3); 
		
		rs.next();
		
		String action2 = rs.getString(1);
		
		Assert.assertEquals(action2 ,"CREATE" );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action2 +"]"+"["
		+"CREATE"+"]");
		
		
		rs= sm1.executeQuery(query4); 
		
		rs.next();
		
		updatedCodeLoaderSampleData_1_HCPCS = rs.getString(1);
		
		Assert.assertEquals(updatedCodeLoaderData_1_HCPCS ,updatedSampleRawData_HCPCS_1);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_1_HCPCS+"]"+"["
		+updatedSampleRawData_HCPCS_1+"]");
		
		rs= sm1.executeQuery(query5); 
		
		rs.next();
		
		updatedCodeLoaderSampleData_2_HCPCS = rs.getString(1);
		
		Assert.assertEquals(updatedCodeLoaderData_2_HCPCS ,updatedSampleRawData_HCPCS_2);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_2_HCPCS+"]"+"["
		+updatedSampleRawData_HCPCS_2+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_05")
	
	public Object[][] testData5() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS",20,24,3);
		
		return data;
		
		}
	
	
	
	@Test(priority=006,description="CodeLoader_12_verifyUpdate_04",dataProvider="HCPCS_TestCase_06")
	
	public void CodeLoader_12_verifyUpdate_04(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_12_verifyUpdate_04");
		
		if (inputFileCount_HCPCS == 2){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String updatedPayorSampleData_1_HCPCS = rs.getString(1);
			
			Assert.assertEquals(updatedPayorSampleData_1_HCPCS ,updatedCodeLoaderData_1_HCPCS);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedPayorSampleData_1_HCPCS+"]"+"["
			+updatedCodeLoaderData_1_HCPCS+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			String updatedPayorSampleData_2_HCPCS = rs.getString(1);
			
			Assert.assertEquals(updatedPayorSampleData_2_HCPCS,updatedCodeLoaderData_2_HCPCS);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+updatedCodeLoaderSampleData_2_HCPCS+"]"+"["
			+updatedCodeLoaderData_2_HCPCS+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_06")
	
	public Object[][] testData6() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS", 25, 26, 3);
		
		return data;
		
		}	
	
	@Test(priority=007,description="CodeLoader_23_verifyDelete_01",dataProvider="HCPCS_TestCase_07")
	
	public void CodeLoader_23_verifyDelete_01(String query1,String query2,String query3) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_23_verifyDelete_01");
		
		if (inputFileCount_HCPCS == 3){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String action = rs.getString(1);
			
			Assert.assertEquals(action ,"TERMINATE");
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action+"]"+"["
			+"TERMINATE"+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			int deleteCount= rs.getInt(1);
			
			Assert.assertEquals(deleteCount,3);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+deleteCount+"]"+"["
			+3+"]");
			
			rs= sm1.executeQuery(query3); 
			
			rs.next();
			
			String effEndDate = rs.getString(1);
			
			Assert.assertNotEquals(effEndDate ,null);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+effEndDate+"]"+"["
			+null+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_07")
	
	public Object[][] testData7() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS", 32,34,3);
		
		return data;
		
		}	
	
	@Test(priority=8,description="CodeLoader_23_verifyDelete_02",dataProvider="HCPCS_TestCase_08")
	
	public void CodeLoader_23_verifyDelete_02(String query1) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_23_verifyDelete_02");
		
		if (inputFileCount_HCPCS == 3){
				
			rs= sm2.executeQuery(query1); 
			
			rs.next();
			
			String effEndDate = rs.getString(1);
			
			Assert.assertNotEquals(effEndDate ,null);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+effEndDate+"]"+"["
			+null+"]");
		
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_08")
	
	public Object[][] testData8() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS", 35,35,3);
		
		return data;
		
		}	
	
	@Test(priority=9,description="CodeLoader_23_verifyDelete_03",dataProvider="HCPCS_TestCase_09")
	
	public void CodeLoader_23_verifyDelete_03(String query1,String query2,String query3,String query4) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_23_verifyDelete_03");
		
		if (inputFileCount_HCPCS == 3){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String action = rs.getString(1);
			
			Assert.assertEquals(action ,"CREATE");
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action+"]"+"["
			+"CREATE"+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			createDeleteValue_CodeLoader= rs.getString(1);
			
			Assert.assertEquals(createDeleteValue_CodeLoader,createDeleteSampleRawData_HCPCS_1);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+createDeleteValue_CodeLoader+"]"+"["
			+createDeleteSampleRawData_HCPCS_1+"]");
			
			rs= sm1.executeQuery(query3); 
			
			rs.next();
			
			String action2 = rs.getString(1);
			
			Assert.assertEquals(action ,"CREATE");
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action2+"]"+"["
			+"CREATE"+"]");
			
			rs= sm1.executeQuery(query4); 
			
			rs.next();
			
			createDeleteValue_CodeLoader_2= rs.getString(1);
			
			Assert.assertEquals(createDeleteValue_CodeLoader,createDeleteSampleRawData_HCPCS_2);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+createDeleteValue_CodeLoader_2+"]"+"["
			+createDeleteSampleRawData_HCPCS_2+"]");
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_09")
	
	public Object[][] testData9() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS", 36,39,3);
		
		return data;
		
		}
	

	@Test(priority=10,description="CodeLoader_23_verifyDelete_04",dataProvider="HCPCS_TestCase_10")
	
	public void CodeLoader_23_verifyDelete_04(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(HCPCSTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_23_verifyDelete_04");
		
		if (inputFileCount_HCPCS == 3){
		
			rs= sm1.executeQuery(query1); 
			
			rs.next();
			
			String createDeleteValue_Payor= rs.getString(1);
			
			Assert.assertEquals(createDeleteValue_Payor,createDeleteValue_CodeLoader);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+createDeleteValue_Payor+"]"+"["
			+createDeleteValue_CodeLoader+"]");
			
			rs= sm1.executeQuery(query2); 
			
			rs.next();
			
			String createDeleteValue_2_Payor= rs.getString(1);
			
			Assert.assertEquals(createDeleteValue_2_Payor,createDeleteValue_CodeLoader_2);
			
			logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+createDeleteValue_2_Payor+"]"+"["
			+createDeleteValue_CodeLoader_2+"]");
		}
		
		logger.endTest();
	
	}
	
	@DataProvider(name="HCPCS_TestCase_10")
	
	public Object[][] testData10() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "HCPCS", 40,41,3);
		
		return data;
		
		}
	
}