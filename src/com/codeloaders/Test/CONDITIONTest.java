package com.codeloaders.Test;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.codeloaders.utility.CodeTypesUtility;
import com.codeloaders.utility.ExcelUtility;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class CONDITIONTest {

	
	ExtentReports logger;

    InputStream propertyFile = CPTTest.class.getResourceAsStream("/CodeLoaders_Configuration.properties");

	Properties property= new Properties();
	
	Connection codeLoaderDB;
	
	Connection payorDB;
	
	Statement sm1,sm2;
	
	ResultSet rs;
	
	long inputFileCount_CPT =0;
	
	int rowCountRawDataFile =0;
	
	List<String> sampleRawData_CPT=null; 
	
	List<String> updatedSampleRawData_CPT=null; 
	
	List<String> updatedSampleRawData_CPT_1=null; 
	
	List<String> updatedSampleRawData_CPT_2=null; 
	
	List<String> createDeleteSampleRawData_CPT_1=null; 
	
	List<String> createDeleteSampleRawData_CPT_2=null; 
	
	int rowCountCodeLoaderDB_CPT=0;
	
	int rowCountCodeLoaderDB_CPT_updated=0;
	
	int rowCountCodeLoaderDB_CPT_updated_2=0;
	
	int rowCountCodeLoader_CPT_updated=0;
	
	String sampleData_CodeLoaderDB_CPT =null;
	
	String updatedDataCodeloader_CPT =null;
	
	String updatedCodeLoaderData_1_CPT =null;
	
	String updatedCodeLoaderData_2_CPT =null;
	
	String  updatedCodeLoaderSampleData_1_CPT =null;
	
	String  updatedCodeLoaderSampleData_2_CPT =null;
	
	String createDeleteValue_CodeLoader=null;
	
	String createDeleteValue_CodeLoader_2=null;
	
	@BeforeClass
	
	public void inputDependencies_CPT() throws Exception{
		
		property.load(propertyFile);
		
		File inputFile_CPT = new File("inputFileCPT");
		
		inputFileCount_CPT =inputFile_CPT.length();
		
		rowCountRawDataFile = CodeTypesUtility.getRowCount_CPT(property.getProperty("inputFile_CPT"));
		
		sampleRawData_CPT = CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_CPT"), 1);
		
		 updatedSampleRawData_CPT=CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_2_CPT"), 1);
		
		 updatedSampleRawData_CPT_1=CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_2_CPT"), 2); 
		
		 updatedSampleRawData_CPT_2=CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_2_CPT"), 2); 
		 
		 createDeleteSampleRawData_CPT_1 =CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_3_CPT"), 1); 
		 
		 createDeleteSampleRawData_CPT_2=CodeTypesUtility.getLine_CPT(property.getProperty("inputFile_3_CPT"), 2); 
		
		codeLoaderDB =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","codeloaderdb","vega");  
		
		payorDB = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle12c","payordb","vega");
		
		 sm1 = codeLoaderDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
		
		 sm2 = payorDB.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		        ResultSet.CONCUR_UPDATABLE);
	}
	
		
	@Test(priority=001,description="CodeLoader_2_verifyCreate_01",dataProvider="CPT_TestCase_01")
	
	public void CodeLoader_2_verifyCreate_01(String query1,String query2,String query3) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_2_verifyCreate_01");
		
		if (inputFileCount_CPT == 1){
		
		rs= sm1.executeQuery(query1); 
		
		rs.next();
		
		rowCountCodeLoaderDB_CPT = rs.getInt(1);
		
		Assert.assertEquals(rowCountCodeLoaderDB_CPT, rowCountRawDataFile);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_CPT+"]"+"["
		+rowCountRawDataFile+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
		String action = rs.getString(1);
		
		Assert.assertEquals(action, "CREATE");
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+action+"]"+"["
				+"CREATE"+"]");
	
		rs = sm1.executeQuery(query2);
		
		rs.next();
		
	   sampleData_CodeLoaderDB_CPT = rs.getString(1);
		
		Assert.assertEquals(sampleData_CodeLoaderDB_CPT,sampleRawData_CPT );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+sampleData_CodeLoaderDB_CPT+"]"+"["
				+sampleRawData_CPT+"]");
		
		}
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_01")
	
	public Object[][] testData1() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT", 3,5,3);
		
		return data;
		
		}
	
	@Test(priority=002,description="CodeLoader_2_verifyCreate_02",dataProvider="CPT_TestCase_02")
	
	public void CodeLoader_2_verifyCreate_02(String query1,String query2) throws Exception{
	
		logger = ExtentReports.get(CPTTest.class);
		
		logger.init(property.getProperty("ExtentReportsPath"), false);
		
		logger.startTest("CodeLoader_2_verifyCreate_02");
		
		if (inputFileCount_CPT == 1){
		
		rs= sm2.executeQuery(query1); 
		
		rs.next();
		
		int rowCountPayorDB_CPT = rs.getInt(1);
		
		Assert.assertEquals(rowCountPayorDB_CPT , rowCountCodeLoaderDB_CPT);
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+rowCountCodeLoaderDB_CPT+"]"+"["
		+rowCountCodeLoaderDB_CPT+"]");
	
		rs = sm2.executeQuery(query2);
		
		rs.next();
		
	   String sampleData_PayorDB_CPT = rs.getString(1);
		
		Assert.assertEquals(sampleData_PayorDB_CPT,sampleData_CodeLoaderDB_CPT );
		
		logger.log(LogStatus.INFO, "The Actual and Expected Value respectively is : "+ "["+sampleData_CodeLoaderDB_CPT+"]"+"["
				+sampleData_CodeLoaderDB_CPT+"]");
		
		}
		logger.endTest();
	
	}
	
	@DataProvider(name="CPT_TestCase_02")
	
	public Object[][] testData2() throws Exception	{
		
		Object[][] data = ExcelUtility.getTestData(property.getProperty("TestDataPath"), "CPT",6,7,3);
		
		return data;
		
		}
	
}
